import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User, UserRole } from './entities/user.entity';
import { Repository } from 'typeorm';

describe('UserService', () => {
  let service: UserService;
  let userRepository: Repository<User>;

  const createUserDto = {
    email: 'test@email.com',
    password: '12345678',
  };

  const dbUSer = {
    id: 1,
    email: 'test@email.com',
    role: UserRole.CUSTOMER,
    password: '12345678',
    firstName: 'FirstName',
    lastName: 'LastName',
    phone_number: '+380666666666',
  };

  const updateResult = { affected: 1, raw: true, generatedMaps: [] };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
    userRepository = module.get(getRepositoryToken(User));

    jest
      .spyOn(userRepository, 'create')
      .mockImplementation(jest.fn(() => dbUSer));

    jest
      .spyOn(userRepository, 'save')
      .mockImplementation(jest.fn(async () => dbUSer));

    jest
      .spyOn(userRepository, 'update')
      .mockImplementation(jest.fn(async () => updateResult));

    jest
      .spyOn(userRepository, 'findOne')
      .mockImplementation(jest.fn(async () => dbUSer));

    jest
      .spyOn(userRepository, 'find')
      .mockImplementation(jest.fn(async () => [dbUSer]));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create method', () => {
    it('should call create method of userRepository', async () => {
      await service.create(createUserDto);

      expect(userRepository.create).toHaveBeenCalledWith(createUserDto);
    });

    it('should call save method of userService', async () => {
      await service.create(createUserDto);

      expect(userRepository.save).toHaveBeenCalledWith(dbUSer);
    });
  });

  describe('update method', () => {
    it('should call update method of userRepository', async () => {
      await service.update(dbUSer, dbUSer.id);

      expect(userRepository.update).toHaveBeenCalledWith(
        { id: dbUSer.id },
        {
          email: dbUSer.email,
          phone_number: dbUSer.phone_number,
          first_name: dbUSer.firstName,
          last_name: dbUSer.lastName,
        },
      );
    });
  });

  describe('findOneByEmail method', () => {
    it('should call findOne method of userRepository', async () => {
      await service.findOneByEmail('test@email.com');

      expect(userRepository.findOne).toHaveBeenCalledWith({
        where: { email: 'test@email.com' },
      });
    });
  });

  describe('findOneById method', () => {
    it('should call findOne method of userRepository', async () => {
      await service.findOneById(1);

      expect(userRepository.findOne).toHaveBeenCalledWith({
        where: { id: 1 },
        relations: ['cards'],
      });
    });
  });

  describe('findAll method', () => {
    it('should call find method of userRepository', async () => {
      await service.findAll();

      expect(userRepository.find).toHaveBeenCalledWith({
        select: ['id', 'email', 'first_name', 'last_name', 'phone_number'],
        relations: ['orders'],
      });
    });
  });
});
