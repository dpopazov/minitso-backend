import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

describe('UserController', () => {
  let controller: UserController;
  let userService: UserService;

  const updateUserDto = {
    id: 1,
    email: 'test@email.com',
    firstName: 'FirstName',
    lastName: 'LastName',
    phone_number: '+380666666666',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
    userService = module.get<UserService>(UserService);

    jest
      .spyOn(userService, 'update')
      .mockImplementation(jest.fn(async () => updateUserDto));

    jest
      .spyOn(userService, 'findAll')
      .mockImplementation(jest.fn(async () => [updateUserDto]));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('update method', () => {
    it('should call update method of userService', async () => {
      await controller.update(updateUserDto, updateUserDto.id, {
        user: updateUserDto,
      });

      expect(userService.update).toHaveBeenCalledWith(
        updateUserDto,
        updateUserDto.id,
      );
    });
  });

  describe('getAll method', () => {
    it('should call findAll method of userService', async () => {
      await controller.getAll();

      expect(userService.findAll).toHaveBeenCalled();
    });
  });
});
