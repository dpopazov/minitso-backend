import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    try {
      const userEntity = this.userRepository.create(createUserDto);

      return await this.userRepository.save(userEntity);
    } catch (err) {
      if (/(email)[\s\S]+(already exists)/.test(err.detail)) {
        throw new BadRequestException(
          'Account with this email already exists.',
        );
      }
    }
  }

  async update(updateUserDto: UpdateUserDto, userId: number) {
    const { email, phone_number, firstName, lastName, stripe_id } =
      updateUserDto;

    try {
      const { affected } = await this.userRepository.update(
        { id: userId },
        {
          ...(email && { email }),
          ...(phone_number && { phone_number }),
          ...(firstName && { first_name: firstName }),
          ...(lastName && { last_name: lastName }),
          ...(stripe_id && { stripe_id }),
        },
      );

      if (affected) {
        const { password, ...user } = await this.findOneById(userId);

        return user;
      }
    } catch (err) {
      if (/(email)[\s\S]+(already exists)/.test(err.detail)) {
        throw new BadRequestException(
          'Account with this email already exists.',
        );
      }
    }
  }

  async findOneByEmail(email: string): Promise<User> {
    return await this.userRepository.findOne({ where: { email } });
  }

  async findOneById(id: number): Promise<User> {
    return await this.userRepository.findOne({
      where: { id },
      relations: ['cards'],
    });
  }

  async findAll(): Promise<User[]> {
    return await this.userRepository.find({
      select: ['id', 'email', 'first_name', 'last_name', 'phone_number'],
      relations: ['orders'],
    });
  }
}
