import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { SALT_ROUND } from '../../config/constants';
import { Order } from '../../order/entities/order.entity';
import { Card } from '../../card/entities/card.entity';

export enum UserRole {
  ADMIN = 'admin',
  CUSTOMER = 'customer',
}

@Entity()
export class User {
  constructor(user?: Partial<User>) {
    if (user) {
      Object.assign(this, user);
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 100, unique: true })
  email: string;

  @Column({ type: 'varchar', length: 255 })
  password?: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.CUSTOMER,
  })
  role?: UserRole;

  @Column({ type: 'varchar', length: 255, nullable: true })
  first_name?: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  last_name?: string;

  @Column({ type: 'varchar', length: 20, nullable: true })
  phone_number?: string;

  @OneToMany(() => Order, (order) => order.user)
  orders?: Order[];

  @Column({ type: 'varchar', length: 255, nullable: true })
  stripe_id?: string;

  @OneToMany(() => Card, (card) => card.user, { cascade: true })
  cards?: Card[];

  @CreateDateColumn()
  created_at?: Date;

  @UpdateDateColumn()
  updated_at?: Date;

  @BeforeInsert()
  hashPassword?() {
    this.password = bcrypt.hashSync(this.password, SALT_ROUND);
  }
}
