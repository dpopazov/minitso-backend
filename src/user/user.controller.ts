import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Req,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../common/roles.guard';
import { Roles } from '../common/roles.decorator';
import { User, UserRole } from './entities/user.entity';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Patch(':id')
  async update(
    @Body() updateUserDto: UpdateUserDto,
    @Param('id', ParseIntPipe) userId: number,
    @Req() req: any,
  ) {
    if (req.user.id !== userId) {
      throw new BadRequestException('You can edit only your information');
    }

    return await this.userService.update(updateUserDto, userId);
  }

  @Get('list')
  @Roles(UserRole.ADMIN)
  async getAll(): Promise<User[]> {
    return await this.userService.findAll();
  }
}
