import { Test, TestingModule } from '@nestjs/testing';
import { CategoryService } from './category.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
import { Repository } from 'typeorm';

describe('CategoryService', () => {
  let service: CategoryService;
  let categoryRepository: Repository<Category>;

  const ids = [1, 2, 3];

  const categories = [
    {
      id: 1,
      name: 'Test category 1',
    },
    {
      id: 2,
      name: 'Test category 2',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CategoryService,
        {
          provide: getRepositoryToken(Category),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<CategoryService>(CategoryService);
    categoryRepository = module.get(getRepositoryToken(Category));

    jest
      .spyOn(categoryRepository, 'findByIds')
      .mockImplementation(jest.fn(async () => categories));

    jest
      .spyOn(categoryRepository, 'find')
      .mockImplementation(jest.fn(async () => categories));

    jest
      .spyOn(categoryRepository, 'save')
      .mockImplementation(jest.fn(async () => categories[0]));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findMany method', () => {
    it('should call findByIds method of categoryRepository', async () => {
      await service.findMany(ids);

      expect(categoryRepository.findByIds).toHaveBeenCalledWith(ids);
    });
  });

  describe('findAll method', () => {
    it('should call find method of categoryRepository', async () => {
      await service.findAll();

      expect(categoryRepository.find).toHaveBeenCalled();
    });
  });

  describe('create method', () => {
    it('should call find method of categoryRepository', async () => {
      await service.create(categories[0]);

      expect(categoryRepository.save).toHaveBeenCalledWith(categories[0]);
    });
  });
});
