import { Test, TestingModule } from '@nestjs/testing';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
import { Repository } from 'typeorm';

describe('CategoryController', () => {
  let controller: CategoryController;
  let categoryService: CategoryService;

  const categories = [
    {
      id: 1,
      name: 'Test category 1',
    },
    {
      id: 2,
      name: 'Test category 2',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CategoryController],
      providers: [
        CategoryService,
        {
          provide: getRepositoryToken(Category),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<CategoryController>(CategoryController);
    categoryService = module.get<CategoryService>(CategoryService);

    jest
      .spyOn(categoryService, 'findAll')
      .mockImplementation(jest.fn(async () => categories));

    jest
      .spyOn(categoryService, 'create')
      .mockImplementation(jest.fn(async () => categories[0]));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getCategories method', () => {
    it('should call findAll method of categoryService', async () => {
      await controller.getCategories();

      expect(categoryService.findAll).toHaveBeenCalled();
    });
  });

  describe('create method', () => {
    it('should call create method of categoryService', async () => {
      await controller.create(categories[0]);

      expect(categoryService.create).toHaveBeenCalledWith(categories[0]);
    });
  });
});
