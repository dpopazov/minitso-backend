const config = {
  development: {
    jwtConfig: {
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '24h' },
    },
    stripeSecretKey: process.env.STRIPE_SECRET_KEY,
  },
  test: {
    jwtConfig: {
      secret: 'secret',
      signOptions: { expiresIn: '24h' },
    },
    stripeSecretKey: 'secret',
  },
};

export default config[process.env.NODE_ENV || 'development'];
