import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getConnectionOptions } from 'typeorm';
import { ProductModule } from './product/product.module';
import { LabelModule } from './label/label.module';
import { CategoryModule } from './category/category.module';
import { OrderModule } from './order/order.module';
import { CardModule } from './card/card.module';
import { ModifierModule } from './modifier/modifier.module';
import { TransactionModule } from './transaction/transaction.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: async () => {
        const options = await getConnectionOptions();

        return Object.assign(options, {
          autoLoadEntities: true,
          entities: [`${__dirname}/**/*.entity{.ts,.js}`],
          migrations: [`${__dirname}/src/db/migrations/*`],
        });
      },
    }),
    UserModule,
    AuthModule,
    ProductModule,
    LabelModule,
    CategoryModule,
    OrderModule,
    CardModule,
    ModifierModule,
    TransactionModule,
  ],
})
export class AppModule {}
