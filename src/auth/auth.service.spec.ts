import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule, JwtService } from '@nestjs/jwt';
import config from '../config/config';
import { PassportModule } from '@nestjs/passport';
import { UserService } from '../user/user.service';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User, UserRole } from '../user/entities/user.entity';
import { Repository } from 'typeorm';
import { CardService } from '../card/card.service';
import { Card } from '../card/entities/card.entity';

describe('AuthService', () => {
  let service: AuthService;
  let userService: UserService;
  let jwtService: JwtService;

  const dbUSer = {
    id: 1,
    email: 'test@email.com',
    password: '12345678',
    role: UserRole.CUSTOMER,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      imports: [JwtModule.register(config.jwtConfig), PassportModule],
      providers: [
        AuthService,
        UserService,
        LocalStrategy,
        JwtStrategy,
        CardService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Card),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    userService = module.get<UserService>(UserService);
    jwtService = module.get<JwtService>(JwtService);

    jest
      .spyOn(userService, 'findOneByEmail')
      .mockImplementation(jest.fn(async () => dbUSer));

    jest.spyOn(jwtService, 'sign').mockImplementation(jest.fn(() => 'token'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('validateUser method', () => {
    it('should call findOneByEmail method of userService with email', async () => {
      const email = 'test@email.com';
      const password = '12345678';

      await service.validateUser(email, password);

      expect(userService.findOneByEmail).toHaveBeenCalledWith(email);
    });
  });

  describe('login method', () => {
    it('should call sign method of jwtService', async () => {
      await service.login(dbUSer);

      expect(jwtService.sign).toHaveBeenCalledWith({
        email: dbUSer.email,
        sub: dbUSer.id,
        role: dbUSer.role,
      });
    });
  });
});
