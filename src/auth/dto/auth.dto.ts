import { PartialType } from '@nestjs/mapped-types';
import { User } from '../../user/entities/user.entity';
import { UpdateUserDto } from '../../user/dto/update-user.dto';
import { Card } from '../../card/entities/card.entity';

export class RegisterAuthDto extends PartialType(User) {
  access_token: string;
  card?: Card;
}

export class LoginAuthDto {
  user: RegisterAuthDto | UpdateUserDto;
}
