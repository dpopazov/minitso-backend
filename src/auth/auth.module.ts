import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { UserModule } from '../user/user.module';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import config from '../config/config';
import { JwtStrategy } from './jwt.strategy';
import { CardModule } from '../card/card.module';

@Module({
  controllers: [AuthController],
  imports: [
    JwtModule.register(config.jwtConfig),
    UserModule,
    CardModule,
    PassportModule,
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
})
export class AuthModule {}
