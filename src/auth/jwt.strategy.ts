import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import config from '../config/config';
import { Request } from 'express';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (req: Request) => {
          if (req.cookies && req.cookies[process.env.AUTHORIZATION_COOKIE]) {
            return req.cookies[process.env.AUTHORIZATION_COOKIE];
          }

          return null;
        },
      ]),
      ignoreExpiration: false,
      secretOrKey: config.jwtConfig.secret,
    });
  }

  async validate(payload: any) {
    return { id: payload.sub, email: payload.email, role: payload.role };
  }
}
