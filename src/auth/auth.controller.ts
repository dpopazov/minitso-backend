import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { UserService } from '../user/user.service';
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './auth.service';
import { LoginAuthDto, RegisterAuthDto } from './dto/auth.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { CardService } from '../card/card.service';

@Controller()
export class AuthController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly cardService: CardService,
  ) {}

  @Post('register')
  async register(
    @Body() createUserDto: CreateUserDto,
  ): Promise<RegisterAuthDto> {
    const user = await this.userService.create(createUserDto);
    const access_token = await this.authService.login(user);

    const { password, ...restUser } = user;

    return { ...restUser, access_token };
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req): Promise<LoginAuthDto> {
    const access_token = await this.authService.login(req.user);

    return {
      user: {
        ...req.user,
        access_token,
      },
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('auth')
  async auth(@Request() req): Promise<LoginAuthDto> {
    const {
      id,
      role,
      email,
      last_name: lastName,
      first_name: firstName,
      phone_number,
    } = await this.userService.findOneById(req.user.id);

    const card = await this.cardService.findOneDefaultByUserId(id);

    return {
      user: { id, role, email, lastName, firstName, phone_number, card },
    };
  }
}
