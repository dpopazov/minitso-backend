import { Test, TestingModule } from '@nestjs/testing';
import { JwtModule } from '@nestjs/jwt';
import config from '../config/config';
import { AuthController } from './auth.controller';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { UserService } from '../user/user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User, UserRole } from '../user/entities/user.entity';
import { CardService } from '../card/card.service';
import { Card } from '../card/entities/card.entity';

describe('AuthController', () => {
  let controller: AuthController;
  let userService: UserService;
  let authService: AuthService;
  let cardService: CardService;

  const createUserDto = {
    email: 'test@email.com',
    password: '12345678',
  };

  const dbUSer = {
    id: 1,
    email: 'test@email.com',
    role: UserRole.CUSTOMER,
  };

  const accessToken = 'test_token';

  const card = {
    id: 1,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      imports: [JwtModule.register(config.jwtConfig), PassportModule],
      providers: [
        AuthService,
        UserService,
        LocalStrategy,
        JwtStrategy,
        CardService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Card),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    userService = module.get<UserService>(UserService);
    authService = module.get<AuthService>(AuthService);
    cardService = module.get<CardService>(CardService);

    jest
      .spyOn(userService, 'create')
      .mockImplementation(jest.fn(async () => dbUSer));

    jest
      .spyOn(userService, 'findOneById')
      .mockImplementation(jest.fn(async () => dbUSer));

    jest
      .spyOn(authService, 'login')
      .mockImplementation(jest.fn(async () => accessToken));

    jest
      .spyOn(cardService, 'findOneDefaultByUserId')
      .mockImplementation(jest.fn(async () => card));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('register method', () => {
    it('should call create method of userService with dto', async () => {
      await controller.register(createUserDto);

      expect(userService.create).toHaveBeenCalledWith(createUserDto);
    });

    it('should call login method of authService with user from db', async () => {
      await controller.register(createUserDto);

      expect(authService.login).toHaveBeenCalledWith(dbUSer);
    });
  });

  describe('login method', () => {
    it('should call login method of authService with user from request', async () => {
      await controller.login({ user: createUserDto });

      expect(authService.login).toHaveBeenCalledWith(createUserDto);
    });
  });

  describe('auth method', () => {
    it('should call findOneById method of userService with user from request', async () => {
      await controller.auth({ user: dbUSer });

      expect(userService.findOneById).toHaveBeenCalledWith(dbUSer.id);
    });

    it('should call findOneDefaultByUserId method of cardService', async () => {
      await controller.auth({ user: dbUSer });

      expect(cardService.findOneDefaultByUserId).toHaveBeenCalledWith(
        dbUSer.id,
      );
    });
  });
});
