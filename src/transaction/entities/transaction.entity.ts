import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Order } from '../../order/entities/order.entity';

export enum TransactionStatus {
  succeeded = 'succeeded',
  pending = 'pending',
  failed = 'failed',
}

@Entity()
export class Transaction {
  constructor(transaction?: Transaction) {
    Object.assign(this, transaction);
  }

  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ type: 'varchar', length: 255 })
  stripe_id?: string;

  @Column({
    type: 'enum',
    enum: TransactionStatus,
    default: TransactionStatus.pending,
  })
  status?: TransactionStatus;

  @ManyToOne(() => Order, (order) => order.transactions)
  @JoinColumn({ name: 'order_id', referencedColumnName: 'id' })
  order?: Order;

  @CreateDateColumn()
  created_at?: Date;

  @UpdateDateColumn()
  updated_at?: Date;
}
