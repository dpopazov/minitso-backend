import { Order } from '../../order/entities/order.entity';
import { IsNotEmpty } from 'class-validator';

export class CreateTransactionDto {
  @IsNotEmpty()
  stripe_id: string;

  @IsNotEmpty()
  status: string;

  @IsNotEmpty()
  order: Order;
}
