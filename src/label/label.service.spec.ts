import { Test, TestingModule } from '@nestjs/testing';
import { LabelService } from './label.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Label } from './entities/label.entity';
import { Repository } from 'typeorm';

describe('LabelService', () => {
  let service: LabelService;
  let labelRepository: Repository<Label>;

  const ids = [1, 2, 3];

  const labels = [
    {
      id: 1,
      name: 'Test label 1',
    },
    {
      id: 2,
      name: 'Test label 2',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LabelService,
        {
          provide: getRepositoryToken(Label),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<LabelService>(LabelService);
    labelRepository = module.get(getRepositoryToken(Label));

    jest
      .spyOn(labelRepository, 'findByIds')
      .mockImplementation(jest.fn(async () => labels));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findMany method', () => {
    it('should call findByIds method of labelRepository', async () => {
      await service.findMany(ids);

      expect(labelRepository.findByIds).toHaveBeenCalledWith(ids);
    });
  });
});
