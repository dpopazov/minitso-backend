import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { Product } from './entities/product.entity';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../common/roles.decorator';
import { UserRole } from '../user/entities/user.entity';
import { RolesGuard } from '../common/roles.guard';
import { UpdateProductDto } from './dto/update-product.dto';
import { LabelService } from '../label/label.service';
import { CategoryService } from '../category/category.service';

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('product')
export class ProductController {
  constructor(
    private readonly productService: ProductService,
    private readonly labelService: LabelService,
    private readonly categoryService: CategoryService,
  ) {}

  @Get('list')
  async findAll(
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe)
    limit: number,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe)
    page: number,
  ): Promise<Product[]> {
    const skip = (page - 1) * limit;
    const [products] = await this.productService.findAndCount({
      take: limit,
      skip,
    });

    return products;
  }

  @Get('view/:id')
  async findOne(@Param('id', ParseIntPipe) id: number): Promise<Product> {
    const product = await this.productService.findOne(id);

    if (!product) {
      throw new NotFoundException();
    }

    return product;
  }

  @Post()
  @Roles(UserRole.ADMIN)
  async createProduct(
    @Body() createProductDto: CreateProductDto,
  ): Promise<Product> {
    return await this.productService.create(createProductDto);
  }

  @Patch(':id')
  @Roles(UserRole.ADMIN)
  async updateProduct(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateProductDto: UpdateProductDto,
  ): Promise<Product> {
    const { label_object_ids, category_ids, name, description, price } =
      updateProductDto;

    let label_objects;
    if (label_object_ids) {
      label_objects = await this.labelService.findMany(label_object_ids);
    }

    let categories;
    if (category_ids) {
      categories = await this.categoryService.findMany(category_ids);
    }

    return await this.productService.update(id, {
      name,
      description,
      price,
      label_objects,
      categories,
    });
  }
}
