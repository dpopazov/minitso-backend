import { Test, TestingModule } from '@nestjs/testing';
import { ProductService } from './product.service';
import { LabelService } from '../label/label.service';
import { CategoryService } from '../category/category.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { Label } from '../label/entities/label.entity';
import { Category } from '../category/entities/category.entity';

describe('ProductService', () => {
  let service: ProductService;
  let productRepository: Repository<Product>;

  const products = [
    {
      id: 1,
      name: 'Test product 1',
      description: 'test description 1',
      price: 10,
    },
    {
      id: 2,
      name: 'Test product 2',
      description: 'test description 2',
      price: 20,
    },
  ];

  const findAndCountResult: [Product[], number] = [products, 2];

  const pagination = { take: 10, skip: 0 };

  const labels = [
    {
      id: 1,
      name: 'Test label 1',
    },
    {
      id: 2,
      name: 'Test label 2',
    },
  ];

  const categories = [
    {
      id: 1,
      name: 'Test category 1',
    },
    {
      id: 2,
      name: 'Test category 2',
    },
  ];

  const updateProductDto = {
    ...products[0],
    label_objects: labels,
    categories,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductService,
        LabelService,
        CategoryService,
        {
          provide: getRepositoryToken(Product),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Label),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Category),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<ProductService>(ProductService);
    productRepository = module.get(getRepositoryToken(Product));

    jest
      .spyOn(productRepository, 'findAndCount')
      .mockImplementation(jest.fn(async () => findAndCountResult));

    jest
      .spyOn(productRepository, 'findOne')
      .mockImplementation(jest.fn(async () => products[0]));

    jest
      .spyOn(productRepository, 'save')
      .mockImplementation(jest.fn(async () => products[0]));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAndCount method', () => {
    it('should call findAndCount method of productRepository', async () => {
      await service.findAndCount(pagination);

      expect(productRepository.findAndCount).toHaveBeenCalledWith({
        ...pagination,
        relations: ['label_objects', 'categories'],
      });
    });
  });

  describe('findOne method', () => {
    it('should call findOne method of productRepository', async () => {
      await service.findOne(1);

      expect(productRepository.findOne).toHaveBeenCalledWith({
        where: { id: 1 },
        relations: ['label_objects', 'categories'],
      });
    });
  });

  describe('create method', () => {
    it('should call save method of productRepository', async () => {
      await service.create(products[0]);

      expect(productRepository.save).toHaveBeenCalledWith(products[0]);
    });
  });

  describe('update method', () => {
    it('should call save method of productRepository', async () => {
      await service.update(products[0].id, updateProductDto);

      expect(productRepository.save).toHaveBeenCalledWith({
        ...products[0],
        label_objects: updateProductDto.label_objects,
        categories: updateProductDto.categories,
      });
    });
  });
});
