import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Label } from '../../label/entities/label.entity';
import { Category } from '../../category/entities/category.entity';
import { Modifier } from '../../modifier/entities/modifier.entity';

@Entity()
export class Product {
  constructor(product?: Partial<Product>) {
    if (product) {
      Object.assign(this, product);
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255 })
  name: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  image?: string;

  @Column({ type: 'text' })
  description: string;

  @Column({ type: 'int' })
  price: number;

  @ManyToMany(() => Label)
  @JoinTable()
  label_objects?: Label[];

  @ManyToMany(() => Category)
  @JoinTable()
  categories?: Category[];

  @ManyToMany(() => Modifier)
  @JoinTable()
  modifiers?: Modifier[];

  @CreateDateColumn()
  created_at?: Date;

  @UpdateDateColumn()
  updated_at?: Date;
}
