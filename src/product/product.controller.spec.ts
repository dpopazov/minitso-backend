import { Test, TestingModule } from '@nestjs/testing';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { LabelService } from '../label/label.service';
import { CategoryService } from '../category/category.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { Label } from '../label/entities/label.entity';
import { Category } from '../category/entities/category.entity';

describe('ProductController', () => {
  let controller: ProductController;
  let productService: ProductService;
  let labelService: LabelService;
  let categoryService: CategoryService;

  const products = [
    {
      id: 1,
      name: 'Test product 1',
      description: 'test description 1',
      price: 10,
    },
    {
      id: 2,
      name: 'Test product 2',
      description: 'test description 2',
      price: 20,
    },
  ];

  const createProductDto = {
    name: 'Test product 1',
    description: 'test description 1',
    price: 10,
  };

  const updateProductDto = {
    ...products[0],
    label_object_ids: [1, 2],
    category_ids: [1, 2],
  };

  const findAndCountResult: [Product[], number] = [products, 2];

  const labels = [
    {
      id: 1,
      name: 'Test label 1',
    },
    {
      id: 2,
      name: 'Test label 2',
    },
  ];

  const categories = [
    {
      id: 1,
      name: 'Test category 1',
    },
    {
      id: 2,
      name: 'Test category 2',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductController],
      providers: [
        ProductService,
        LabelService,
        CategoryService,
        {
          provide: getRepositoryToken(Product),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Label),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Category),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<ProductController>(ProductController);
    productService = module.get<ProductService>(ProductService);
    labelService = module.get<LabelService>(LabelService);
    categoryService = module.get<CategoryService>(CategoryService);

    jest
      .spyOn(productService, 'findAndCount')
      .mockImplementation(jest.fn(async () => findAndCountResult));

    jest
      .spyOn(productService, 'findOne')
      .mockImplementation(jest.fn(async () => products[0]));

    jest
      .spyOn(productService, 'create')
      .mockImplementation(jest.fn(async () => products[0]));

    jest
      .spyOn(productService, 'update')
      .mockImplementation(jest.fn(async () => products[0]));

    jest
      .spyOn(labelService, 'findMany')
      .mockImplementation(jest.fn(async () => labels));

    jest
      .spyOn(categoryService, 'findMany')
      .mockImplementation(jest.fn(async () => categories));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll method', () => {
    it('should call findAndCount method of productService', async () => {
      await controller.findAll(10, 1);

      expect(productService.findAndCount).toHaveBeenCalledWith({
        skip: 0,
        take: 10,
      });
    });
  });

  describe('findOne method', () => {
    it('should call findOne method of productService', async () => {
      await controller.findOne(products[0].id);

      expect(productService.findOne).toHaveBeenCalledWith(products[0].id);
    });
  });

  describe('createProduct method', () => {
    it('should call create method of productService', async () => {
      await controller.createProduct(createProductDto);

      expect(productService.create).toHaveBeenCalledWith(createProductDto);
    });
  });

  describe('updateProduct method', () => {
    it('should call findMany method of labelService', async () => {
      await controller.updateProduct(products[0].id, updateProductDto);

      expect(labelService.findMany).toHaveBeenCalledWith(
        updateProductDto.label_object_ids,
      );
    });

    it('should call findMany method of categoryService', async () => {
      await controller.updateProduct(products[0].id, updateProductDto);

      expect(categoryService.findMany).toHaveBeenCalledWith(
        updateProductDto.category_ids,
      );
    });

    it('should call update method of productService', async () => {
      await controller.updateProduct(products[0].id, updateProductDto);

      expect(productService.update).toHaveBeenCalledWith(products[0].id, {
        name: products[0].name,
        description: products[0].description,
        price: products[0].price,
        label_objects: labels,
        categories,
      });
    });
  });
});
