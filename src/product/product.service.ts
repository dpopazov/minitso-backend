import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async findAndCount({
    take,
    skip,
  }: FindManyOptions): Promise<[Product[], number]> {
    return await this.productRepository.findAndCount({
      take,
      skip,
      relations: ['label_objects', 'categories'],
    });
  }

  async findOne(id): Promise<Product> {
    return await this.productRepository.findOne({
      where: { id },
      relations: ['label_objects', 'categories'],
    });
  }

  async create(createProductDto: CreateProductDto): Promise<Product> {
    const product = new Product(createProductDto);

    return await this.productRepository.save(product);
  }

  async update(
    productId: number,
    updateProductDto: UpdateProductDto,
  ): Promise<Product> {
    const product = await this.findOne(productId);

    if (!product) {
      throw new NotFoundException("Product with given id doesn't exist");
    }

    const newProduct = new Product({
      ...product,
      ...updateProductDto,
      ...(updateProductDto.label_objects && {
        label_objects: updateProductDto.label_objects,
      }),
      ...(updateProductDto.categories && {
        categories: updateProductDto.categories,
      }),
    });

    return await this.productRepository.save(newProduct);
  }
}
