import { PartialType } from '@nestjs/mapped-types';
import { CreateProductDto } from './create-product.dto';
import { Label } from '../../label/entities/label.entity';
import { Category } from '../../category/entities/category.entity';

export class UpdateProductDto extends PartialType(CreateProductDto) {
  label_object_ids?: number[];

  label_objects?: Label[];

  category_ids?: number[];

  categories?: Category[];
}
