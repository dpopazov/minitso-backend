import { MigrationInterface, QueryRunner } from 'typeorm';

export class CardMigration1634503645747 implements MigrationInterface {
  name = 'CardMigration1634503645747';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "card" ("id" SERIAL NOT NULL, "external_id" character varying(255) NOT NULL, "default" boolean NOT NULL DEFAULT false, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "user_id" integer, CONSTRAINT "PK_9451069b6f1199730791a7f4ae4" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "stripe_id" character varying(255)`,
    );
    await queryRunner.query(
      `ALTER TABLE "card" ADD CONSTRAINT "FK_00ec72ad98922117bad8a86f980" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "card" DROP CONSTRAINT "FK_00ec72ad98922117bad8a86f980"`,
    );
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "stripe_id"`);
    await queryRunner.query(`DROP TABLE "card"`);
  }
}
