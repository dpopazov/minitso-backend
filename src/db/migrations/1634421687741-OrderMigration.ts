import { MigrationInterface, QueryRunner } from 'typeorm';

export class OrderMigration1634421687741 implements MigrationInterface {
  name = 'OrderMigration1634421687741';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "order" ALTER COLUMN "status" SET DEFAULT 'Order received'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "order" ALTER COLUMN "status" SET DEFAULT 'Processing'`,
    );
  }
}
