import { MigrationInterface, QueryRunner } from 'typeorm';

export class LabelMigration1634240281263 implements MigrationInterface {
  name = 'LabelMigration1634240281263';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "label" ("id" SERIAL NOT NULL, "name" character varying(255) NOT NULL, "image" character varying(255), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_5692ac5348861d3776eb5843672" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "product_label_objects_label" ("productId" integer NOT NULL, "labelId" integer NOT NULL, CONSTRAINT "PK_dc89ed7910b1475b7b5ad90ef2d" PRIMARY KEY ("productId", "labelId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_703831e86764a5d885ad029b31" ON "product_label_objects_label" ("productId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_3974ec392676454f601e5db5a5" ON "product_label_objects_label" ("labelId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "product_label_objects_label" ADD CONSTRAINT "FK_703831e86764a5d885ad029b31c" FOREIGN KEY ("productId") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "product_label_objects_label" ADD CONSTRAINT "FK_3974ec392676454f601e5db5a58" FOREIGN KEY ("labelId") REFERENCES "label"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "product_label_objects_label" DROP CONSTRAINT "FK_3974ec392676454f601e5db5a58"`,
    );
    await queryRunner.query(
      `ALTER TABLE "product_label_objects_label" DROP CONSTRAINT "FK_703831e86764a5d885ad029b31c"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_3974ec392676454f601e5db5a5"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_703831e86764a5d885ad029b31"`,
    );
    await queryRunner.query(`DROP TABLE "product_label_objects_label"`);
    await queryRunner.query(`DROP TABLE "label"`);
  }
}
