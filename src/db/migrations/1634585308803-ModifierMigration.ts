import { MigrationInterface, QueryRunner } from 'typeorm';

export class ModifierMigration1634585308803 implements MigrationInterface {
  name = 'ModifierMigration1634585308803';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "modifier" ("id" SERIAL NOT NULL, "name" character varying(255) NOT NULL, "price" integer NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_30c20db2bc7a8c2318b4db0dfc0" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "product_modifiers_modifier" ("productId" integer NOT NULL, "modifierId" integer NOT NULL, CONSTRAINT "PK_e7d391dd577c57947f6f2416405" PRIMARY KEY ("productId", "modifierId"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_0b29f58cb5f9648338c6b6d6a1" ON "product_modifiers_modifier" ("productId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_720eefd25777aa895b408add27" ON "product_modifiers_modifier" ("modifierId") `,
    );
    await queryRunner.query(
      `ALTER TABLE "product_modifiers_modifier" ADD CONSTRAINT "FK_0b29f58cb5f9648338c6b6d6a16" FOREIGN KEY ("productId") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "product_modifiers_modifier" ADD CONSTRAINT "FK_720eefd25777aa895b408add270" FOREIGN KEY ("modifierId") REFERENCES "modifier"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "product_modifiers_modifier" DROP CONSTRAINT "FK_720eefd25777aa895b408add270"`,
    );
    await queryRunner.query(
      `ALTER TABLE "product_modifiers_modifier" DROP CONSTRAINT "FK_0b29f58cb5f9648338c6b6d6a16"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_720eefd25777aa895b408add27"`,
    );
    await queryRunner.query(
      `DROP INDEX "public"."IDX_0b29f58cb5f9648338c6b6d6a1"`,
    );
    await queryRunner.query(`DROP TABLE "product_modifiers_modifier"`);
    await queryRunner.query(`DROP TABLE "modifier"`);
  }
}
