import { MigrationInterface, QueryRunner } from 'typeorm';

export class OrderMigration1634330688138 implements MigrationInterface {
  name = 'OrderMigration1634330688138';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "public"."order_status_enum" AS ENUM('Processing', 'Ready', 'Order received')`,
    );
    await queryRunner.query(
      `ALTER TABLE "order" ADD "status" "public"."order_status_enum" NOT NULL DEFAULT 'Processing'`,
    );
    await queryRunner.query(
      `ALTER TABLE "order" ADD "created_at" TIMESTAMP NOT NULL DEFAULT now()`,
    );
    await queryRunner.query(
      `ALTER TABLE "order" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "updated_at"`);
    await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "created_at"`);
    await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "status"`);
    await queryRunner.query(`DROP TYPE "public"."order_status_enum"`);
  }
}
