import { MigrationInterface, QueryRunner } from 'typeorm';

export class OrderMigration1634674781283 implements MigrationInterface {
  name = 'OrderMigration1634674781283';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "order" ADD "delivery_time" TIMESTAMP`,
    );
    await queryRunner.query(`ALTER TABLE "order" ADD "paid_at" TIMESTAMP`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "paid_at"`);
    await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "delivery_time"`);
  }
}
