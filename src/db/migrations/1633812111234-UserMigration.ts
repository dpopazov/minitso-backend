import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserMigration1633812111234 implements MigrationInterface {
  name = 'UserMigration1633812111234';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "first_name" character varying(255)`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "last_name" character varying(255)`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "phone_number" character varying(20)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "phone_number"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "last_name"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "first_name"`);
  }
}
