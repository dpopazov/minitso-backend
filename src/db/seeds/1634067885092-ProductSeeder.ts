import { MigrationInterface, QueryRunner } from 'typeorm';
import { Product } from '../../product/entities/product.entity';

const defaultProducts = [
  {
    name: 'Sesame Shrimp Test',
    description:
      'Batter-fried shrimp in a sweet sesame sauce w/ broccoli & pineapple, topped with sesame seeds',
    price: 12,
  },
  {
    name: 'Greenbelt Kombucha Peach',
    description:
      'Batter-fried shrimp in a sweet sesame sauce w/ broccoli & pineapple, topped with sesame seeds',
    price: 3,
  },
  {
    name: 'Spicy Green Bean Shrimp',
    description:
      'shrimp, green beans, and carrots in a spicy Szechuan brown sauce',
    price: 10,
  },
];

export class ProductSeeder1634067885092 implements MigrationInterface {
  name = 'ProductSeeder1634067885092';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.connection
      .createQueryBuilder()
      .insert()
      .into(Product)
      .values(defaultProducts)
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.connection
      .createQueryBuilder()
      .delete()
      .from(Product)
      .execute();
  }
}
