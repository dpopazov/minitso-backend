import { MigrationInterface, QueryRunner } from 'typeorm';
import { User, UserRole } from '../../user/entities/user.entity';

const defaultAdmin = {
  email: 'admin@email.com',
  password: '12345678',
  role: UserRole.ADMIN,
};

export class AdminSeeder1634762246190 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const admin = new User(defaultAdmin);

    await queryRunner.connection
      .createQueryBuilder()
      .insert()
      .into(User)
      .values(admin)
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.connection
      .createQueryBuilder()
      .delete()
      .from(User)
      .where({ email: defaultAdmin.email })
      .execute();
  }
}
