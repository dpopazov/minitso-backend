import { MigrationInterface, QueryRunner } from 'typeorm';
import { Category } from '../../category/entities/category.entity';
import { Product } from '../../product/entities/product.entity';

const defaultCategories = [
  {
    name: 'Classics',
  },
  {
    name: 'Drinks',
  },
  {
    name: 'Appetizers',
  },
];

export class CategorySeeder1634245171958 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const categoryEntities = defaultCategories.map(
      (category) => new Category(category),
    );

    await queryRunner.connection
      .createQueryBuilder()
      .insert()
      .into(Category)
      .values(categoryEntities)
      .execute();

    const products = await queryRunner.connection
      .createEntityManager()
      .find(Product);

    products[0].categories = [categoryEntities[0], categoryEntities[2]];
    products[1].categories = [categoryEntities[1]];
    products[2].categories = [categoryEntities[2]];

    await queryRunner.manager.save(Product, products);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.connection
      .createQueryBuilder()
      .delete()
      .from(Category)
      .execute();
  }
}
