import { MigrationInterface, QueryRunner } from 'typeorm';
import { Label } from '../../label/entities/label.entity';
import { Product } from '../../product/entities/product.entity';

const defaultLabels = [
  {
    name: 'Sesame',
  },
  {
    name: 'Nut-free',
  },
  {
    name: 'Dairyfree',
  },
  {
    name: 'Vegetarian',
  },
  {
    name: 'Spicy',
  },
];

export class LabelSeeder1634241342933 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const labelEntities = defaultLabels.map((label) => new Label(label));

    await queryRunner.connection
      .createQueryBuilder()
      .insert()
      .into(Label)
      .values(labelEntities)
      .execute();

    const products = await queryRunner.connection
      .createEntityManager()
      .find(Product);

    products[0].label_objects = [labelEntities[0], labelEntities[1]];
    products[1].label_objects = [
      labelEntities[2],
      labelEntities[3],
      labelEntities[4],
    ];

    await queryRunner.manager.save(Product, products);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.connection
      .createQueryBuilder()
      .delete()
      .from(Label)
      .execute();
  }
}
