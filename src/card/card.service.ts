import { Injectable } from '@nestjs/common';
import { CreateCardDto } from './dto/create-card.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Card } from './entities/card.entity';
import { Repository } from 'typeorm';
import { User } from '../user/entities/user.entity';

@Injectable()
export class CardService {
  constructor(
    @InjectRepository(Card) private readonly cardRepository: Repository<Card>,
  ) {}

  async create(createCardDto: CreateCardDto): Promise<Card> {
    const card = await this.cardRepository.create(createCardDto);

    return await this.cardRepository.save(card);
  }

  async findOneDefaultByUserId(userId: number): Promise<Card> {
    const user = new User({ id: userId });

    return await this.cardRepository.findOne({
      where: { default: true, user },
    });
  }
}
