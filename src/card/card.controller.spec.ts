import { Test, TestingModule } from '@nestjs/testing';
import { CardController } from './card.controller';
import { CardService } from './card.service';
import { UserService } from '../user/user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Card } from './entities/card.entity';
import { User, UserRole } from '../user/entities/user.entity';
import { Repository } from 'typeorm';

jest.mock('stripe', () => {
  return {
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
      customers: {
        create: () => Promise.resolve({ id: 1 }),
        createSource: () => Promise.resolve({ id: 'test' }),
      },
    })),
  };
});

describe('CardController', () => {
  let controller: CardController;
  let userService: UserService;
  let cardService: CardService;

  const token = 'test_token';

  const user = {
    id: 1,
    email: 'test@email.com',
    role: UserRole.CUSTOMER,
    cards: [],
  };

  const card = {
    id: 1,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CardController],
      providers: [
        CardService,
        UserService,
        {
          provide: getRepositoryToken(Card),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<CardController>(CardController);
    userService = module.get<UserService>(UserService);
    cardService = module.get<CardService>(CardService);

    jest
      .spyOn(userService, 'findOneById')
      .mockImplementation(jest.fn(async () => user));

    jest
      .spyOn(userService, 'update')
      .mockImplementation(
        jest.fn(async () => ({ ...user, stripe_id: 'test' })),
      );

    jest
      .spyOn(cardService, 'create')
      .mockImplementation(jest.fn(async () => card));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('create method', () => {
    it('should call findOneById method of userService', async () => {
      await controller.create(token, { user });

      expect(userService.findOneById).toHaveBeenCalledWith(user.id);
    });

    it('should call update method of userService', async () => {
      await controller.create(token, { user });

      expect(userService.update).toHaveBeenCalledWith(
        { stripe_id: 1 },
        user.id,
      );
    });

    it('should call update method of userService', async () => {
      await controller.create(token, { user });

      expect(cardService.create).toHaveBeenCalledWith({
        external_id: 'test',
        user: { ...user, stripe_id: 'test' },
        default: !user.cards.length,
      });
    });
  });
});
