import { Test, TestingModule } from '@nestjs/testing';
import { CardService } from './card.service';
import { UserService } from '../user/user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Card } from './entities/card.entity';
import { Repository } from 'typeorm';
import { User } from '../user/entities/user.entity';

describe('CardService', () => {
  let service: CardService;
  let cardRepository: Repository<Card>;

  const user = {
    id: 1,
    email: 'test@email.com',
  };

  const createCardDto = {
    external_id: 'test',
    user,
    default: true,
  };

  const card = {
    id: 1,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CardService,
        UserService,
        {
          provide: getRepositoryToken(Card),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<CardService>(CardService);
    cardRepository = module.get(getRepositoryToken(Card));

    jest
      .spyOn(cardRepository, 'create')
      .mockImplementation(jest.fn(() => card));

    jest
      .spyOn(cardRepository, 'save')
      .mockImplementation(jest.fn(async () => card));

    jest
      .spyOn(cardRepository, 'findOne')
      .mockImplementation(jest.fn(async () => card));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create method', () => {
    it('should call create method of cardRepository', async () => {
      await service.create(createCardDto);

      expect(cardRepository.create).toHaveBeenCalledWith(createCardDto);
    });

    it('should call save method of cardRepository', async () => {
      await service.create(createCardDto);

      expect(cardRepository.save).toHaveBeenCalledWith(card);
    });
  });

  describe('findOneDefaultByUserId method', () => {
    it('should call findOne method of cardRepository', async () => {
      await service.findOneDefaultByUserId(1);

      expect(cardRepository.findOne).toHaveBeenCalledWith({
        where: { default: true, user: { id: user.id } },
      });
    });
  });
});
