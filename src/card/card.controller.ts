import { Controller, Post, Body, Req, UseGuards } from '@nestjs/common';
import Stripe from 'stripe';
import { CardService } from './card.service';
import { UserService } from '../user/user.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import config from '../config/config';
import { Card } from './entities/card.entity';

const stripe = new Stripe(config.stripeSecretKey, {
  apiVersion: '2020-08-27',
});

@UseGuards(JwtAuthGuard)
@Controller('payment-card')
export class CardController {
  constructor(
    private readonly cardService: CardService,
    private readonly userService: UserService,
  ) {}

  @Post()
  async create(@Body('token') token: string, @Req() req: any): Promise<Card> {
    let user = await this.userService.findOneById(req.user.id);

    if (!user.stripe_id) {
      const customer = await stripe.customers.create({
        email: user.email,
        name: `${user.first_name} ${user.last_name}`,
      });

      user = await this.userService.update({ stripe_id: customer.id }, user.id);
    }

    const source = await stripe.customers.createSource(user.stripe_id, {
      source: token,
    });

    return await this.cardService.create({
      external_id: source.id,
      user,
      default: !user.cards.length,
    });
  }
}
