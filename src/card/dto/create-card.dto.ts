import { IsNotEmpty } from 'class-validator';
import { User } from '../../user/entities/user.entity';

export class CreateCardDto {
  @IsNotEmpty()
  external_id: string;

  @IsNotEmpty()
  user: User;

  default: boolean;
}
