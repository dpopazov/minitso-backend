import { Module } from '@nestjs/common';
import { ModifierService } from './modifier.service';
import { ModifierController } from './modifier.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Modifier } from './entities/modifier.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Modifier])],
  controllers: [ModifierController],
  providers: [ModifierService],
})
export class ModifierModule {}
