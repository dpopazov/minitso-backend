import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Product } from '../../product/entities/product.entity';
import { User } from '../../user/entities/user.entity';
import { Transaction } from '../../transaction/entities/transaction.entity';

export enum OrderStatus {
  received = 'Order received',
  processing = 'Processing',
  ready = 'Ready',
}

@Entity()
export class Order {
  constructor(order?: Partial<Order>) {
    if (order) {
      Object.assign(this, order);
    }
  }

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => Product)
  @JoinTable()
  products?: Product[];

  @ManyToOne(() => User, (user) => user.orders)
  user?: User;

  @Column({
    type: 'enum',
    enum: OrderStatus,
    default: OrderStatus.received,
  })
  status?: OrderStatus;

  @Column({ type: 'timestamp', nullable: true })
  delivery_time?: Date;

  @Column({ type: 'timestamp', nullable: true })
  paid_at?: Date;

  @OneToMany(() => Transaction, (transaction) => transaction.order)
  transactions?: Transaction[];

  @CreateDateColumn()
  created_at?: Date;

  @UpdateDateColumn()
  updated_at?: Date;
}
