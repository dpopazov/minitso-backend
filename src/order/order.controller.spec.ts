import { Test, TestingModule } from '@nestjs/testing';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Order, OrderStatus } from './entities/order.entity';
import { Repository } from 'typeorm';
import { CardService } from '../card/card.service';
import { TransactionService } from '../transaction/transaction.service';
import {
  Transaction,
  TransactionStatus,
} from '../transaction/entities/transaction.entity';
import { Card } from '../card/entities/card.entity';

jest.mock('stripe', () => {
  return {
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
      charges: {
        create: () =>
          Promise.resolve({ id: 1, status: TransactionStatus.succeeded }),
      },
    })),
  };
});

describe('OrderController', () => {
  let controller: OrderController;
  let orderService: OrderService;
  let cardService: CardService;
  let transactionService: TransactionService;

  const user = {
    id: 1,
    email: 'test@email.com',
  };

  const product = {
    id: 1,
    name: 'Test product',
    description: 'test description',
    price: 10,
  };

  const order = {
    id: 1,
    status: OrderStatus.received,
    products: [product],
    user,
  };

  const cart_id = 1;

  const card = {
    id: 1,
    external_id: 'test',
  };

  const transaction = {
    id: 1,
    stripe_id: 'test',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderController],
      providers: [
        OrderService,
        TransactionService,
        CardService,
        {
          provide: getRepositoryToken(Order),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Transaction),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Card),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<OrderController>(OrderController);
    orderService = module.get<OrderService>(OrderService);
    cardService = module.get<CardService>(CardService);
    transactionService = module.get<TransactionService>(TransactionService);

    jest
      .spyOn(orderService, 'findOneCurrentOrderByUserId')
      .mockImplementation(jest.fn(async () => order));

    jest
      .spyOn(orderService, 'createOrder')
      .mockImplementation(jest.fn(async () => order));

    jest
      .spyOn(orderService, 'updateOrder')
      .mockImplementation(jest.fn(async () => order));

    jest
      .spyOn(orderService, 'findOneById')
      .mockImplementation(jest.fn(async () => order));

    jest
      .spyOn(cardService, 'findOneDefaultByUserId')
      .mockImplementation(jest.fn(async () => card));

    jest
      .spyOn(transactionService, 'create')
      .mockImplementation(jest.fn(async () => transaction));
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getCurrentOrder method', () => {
    it('should call findOneCurrentOrderByUserId method of orderService', async () => {
      await controller.getCurrentOrder({ user });

      expect(orderService.findOneCurrentOrderByUserId).toHaveBeenCalledWith(
        user.id,
      );
    });
  });

  describe('addProductToCart method', () => {
    it('should call createOrder method of orderService', async () => {
      await controller.addProductToCart(null, product, { user });

      expect(orderService.createOrder).toHaveBeenCalledWith(user.id);
    });

    it('should call findOneById method of orderService', async () => {
      await controller.addProductToCart(cart_id, product, { user });

      expect(orderService.findOneById).toHaveBeenCalledWith(cart_id);
    });

    it('should call updateOrder method of orderService', async () => {
      await controller.addProductToCart(cart_id, product, { user });

      expect(orderService.updateOrder).toHaveBeenCalledWith({
        id: cart_id,
        products: [product],
      });
    });
  });

  describe('pay method', () => {
    it('should call findOneById method of orderService', async () => {
      await controller.pay(cart_id, order.id, { user });

      expect(orderService.findOneById).toHaveBeenCalledWith(order.id);
    });

    it('should call findOneDefaultByUserId method of cardService', async () => {
      await controller.pay(cart_id, order.id, { user });

      expect(cardService.findOneDefaultByUserId).toHaveBeenCalledWith(user.id);
    });

    it('should call create method of transactionService', async () => {
      await controller.pay(cart_id, order.id, { user });

      expect(transactionService.create).toHaveBeenCalledWith({
        stripe_id: 1,
        status: TransactionStatus.succeeded,
        order,
      });
    });

    it('should call updateOrder method of orderService', async () => {
      await controller.pay(cart_id, order.id, { user });

      expect(orderService.updateOrder).toHaveBeenCalledWith({
        ...order,
        status: OrderStatus.processing,
        paid_at: new Date(),
      });
    });
  });

  describe('updateOrder method', () => {
    beforeEach(() => {
      jest
        .spyOn(orderService, 'findOneById')
        .mockImplementation(
          jest.fn(async () => ({ ...order, status: OrderStatus.processing })),
        );
    });

    it('should call findOneById method of orderService', async () => {
      await controller.updateOrder(order.id, 'processing');

      expect(orderService.findOneById).toHaveBeenCalledWith(order.id);
    });

    it('should call updateOrder method of orderService', async () => {
      await controller.updateOrder(order.id, 'processing');

      expect(orderService.updateOrder).toHaveBeenCalledWith({
        ...order,
        status: OrderStatus.processing,
      });
    });
  });
});
