import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { OrderService } from './order.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Order, OrderStatus } from './entities/order.entity';
import { Product } from '../product/entities/product.entity';
import Stripe from 'stripe';
import config from '../config/config';
import { CardService } from '../card/card.service';
import { TransactionService } from '../transaction/transaction.service';
import { TransactionStatus } from '../transaction/entities/transaction.entity';
import { RolesGuard } from '../common/roles.guard';
import { Roles } from '../common/roles.decorator';
import { UserRole } from '../user/entities/user.entity';

const stripe = new Stripe(config.stripeSecretKey, {
  apiVersion: '2020-08-27',
});

@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('cart')
export class OrderController {
  constructor(
    private readonly orderService: OrderService,
    private readonly cardService: CardService,
    private readonly transactionService: TransactionService,
  ) {}

  @Get()
  async getCurrentOrder(@Req() req: any): Promise<{ order: Order }> {
    const order = await this.orderService.findOneCurrentOrderByUserId(
      req.user.id,
    );

    return { order };
  }

  @Post('product')
  async addProductToCart(
    @Body('cart_id') cart_id: number | null,
    @Body('product') product: Product,
    @Req() req: any,
  ): Promise<Order> {
    let order;

    if (!cart_id) {
      order = await this.orderService.createOrder(req.user.id);
    } else {
      order = await this.orderService.findOneById(cart_id);
    }

    if (order.status !== OrderStatus.received) {
      throw new BadRequestException('You can not modify this order');
    }

    return await this.orderService.updateOrder({
      id: cart_id || order.id,
      products: [new Product(product)],
    });
  }

  @Post('pay')
  async pay(
    @Body('cart_id') cart_id: number,
    @Body('order_id') order_id: number,
    @Req() req: any,
  ) {
    const order = await this.orderService.findOneById(order_id);

    if (req.user.id !== order.user.id) {
      throw new BadRequestException('You cannot pay for this order');
    }

    const card = await this.cardService.findOneDefaultByUserId(req.user.id);

    if (!card) {
      throw new BadRequestException("You don't have payment methods");
    }

    const amount = order.products.reduce((acc, item) => {
      acc += item.price;

      return acc;
    }, 0);

    const charge = await stripe.charges.create({
      source: card.external_id,
      amount: amount * 100,
      currency: 'usd',
      customer: order.user.stripe_id,
      description: `Payment of order ${order.id}`,
    });

    await this.transactionService.create({
      stripe_id: charge.id,
      status: TransactionStatus[charge.status],
      order,
    });

    await this.orderService.updateOrder({
      ...order,
      status: OrderStatus.processing,
      paid_at: new Date(),
    });

    return charge;
  }

  @Patch('order/:id')
  @Roles(UserRole.ADMIN)
  async updateOrder(
    @Param('id', ParseIntPipe) id: number,
    @Body('status') status: string,
  ): Promise<Order> {
    const order = await this.orderService.findOneById(id);

    if (order && order.status === OrderStatus.received) {
      throw new BadRequestException(
        'You can not modify status of unpaid order',
      );
    }

    return await this.orderService.updateOrder({
      ...order,
      status: OrderStatus[status],
    });
  }
}
