import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Order, OrderStatus } from './entities/order.entity';
import { Repository } from 'typeorm';
import { User } from '../user/entities/user.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
  ) {}

  async findOneById(id: number): Promise<Order> {
    return await this.orderRepository.findOne({
      where: { id },
      relations: ['products', 'user'],
    });
  }

  async findOneCurrentOrderByUserId(userId: number): Promise<Order> {
    const result = await this.orderRepository.findOne({
      where: { user: userId, status: OrderStatus.received },
      relations: ['products'],
    });

    if (!result) {
      return { id: null, products: [] };
    }

    return result;
  }

  async createOrder(userId: number): Promise<Order> {
    const order = this.orderRepository.create({
      user: new User({ id: userId }),
    });

    return await this.orderRepository.save(order);
  }

  async updateOrder(order: Order): Promise<Order> {
    const orderDb = await this.orderRepository.findOne({
      where: { id: order.id },
      relations: ['products'],
    });

    if (!orderDb) {
      throw new BadRequestException('Order does not exist');
    }

    const newOrder = new Order({
      ...orderDb,
      ...order,
      products: [...orderDb.products, ...order.products],
    });

    return await this.orderRepository.save(newOrder);
  }
}
