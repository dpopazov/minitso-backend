import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { CardModule } from '../card/card.module';
import { TransactionModule } from '../transaction/transaction.module';

@Module({
  imports: [TypeOrmModule.forFeature([Order]), CardModule, TransactionModule],
  controllers: [OrderController],
  providers: [OrderService],
})
export class OrderModule {}
