import { Test, TestingModule } from '@nestjs/testing';
import { OrderService } from './order.service';
import { TransactionService } from '../transaction/transaction.service';
import { CardService } from '../card/card.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Order, OrderStatus } from './entities/order.entity';
import { Repository } from 'typeorm';
import { Transaction } from '../transaction/entities/transaction.entity';
import { Card } from '../card/entities/card.entity';

describe('OrderService', () => {
  let service: OrderService;
  let orderRepository: Repository<Order>;

  const user = {
    id: 1,
    email: 'test@email.com',
  };

  const product = {
    id: 1,
    name: 'Test product',
    description: 'test description',
    price: 10,
  };

  const order = {
    id: 1,
    status: OrderStatus.received,
    products: [product],
    user,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrderService,
        TransactionService,
        CardService,
        {
          provide: getRepositoryToken(Order),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Transaction),
          useClass: Repository,
        },
        {
          provide: getRepositoryToken(Card),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<OrderService>(OrderService);
    orderRepository = module.get(getRepositoryToken(Order));

    jest
      .spyOn(orderRepository, 'findOne')
      .mockImplementation(jest.fn(async () => order));

    jest
      .spyOn(orderRepository, 'create')
      .mockImplementation(jest.fn(() => order));

    jest
      .spyOn(orderRepository, 'save')
      .mockImplementation(jest.fn(async () => order));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOneById method', () => {
    it('should call findOne method of orderRepository', async () => {
      await service.findOneById(order.id);

      expect(orderRepository.findOne).toHaveBeenCalledWith({
        where: { id: order.id },
        relations: ['products', 'user'],
      });
    });
  });

  describe('findOneCurrentOrderByUserId method', () => {
    it('should call findOne method of orderRepository', async () => {
      await service.findOneCurrentOrderByUserId(user.id);

      expect(orderRepository.findOne).toHaveBeenCalledWith({
        where: { user: user.id, status: OrderStatus.received },
        relations: ['products'],
      });
    });
  });

  describe('createOrder method', () => {
    it('should call create method of orderRepository', async () => {
      await service.createOrder(user.id);

      expect(orderRepository.create).toHaveBeenCalledWith({
        user: { id: user.id },
      });
    });

    it('should call create method of orderRepository', async () => {
      await service.createOrder(user.id);

      expect(orderRepository.save).toHaveBeenCalledWith(order);
    });
  });

  describe('updateOrder method', () => {
    it('should call findOne method of orderRepository', async () => {
      await service.updateOrder(order);

      expect(orderRepository.findOne).toHaveBeenCalledWith({
        where: { id: order.id },
        relations: ['products'],
      });
    });

    it('should call save method of orderRepository', async () => {
      await service.updateOrder(order);

      expect(orderRepository.save).toHaveBeenCalledWith({
        ...order,
        products: [...order.products, ...order.products],
      });
    });
  });
});
