FROM node:14-alpine

EXPOSE 3001

WORKDIR /backend

COPY ["package.json", "yarn.lock", "./"]

RUN yarn

COPY . .

CMD yarn migration:run && yarn seed:run && yarn start:dev
