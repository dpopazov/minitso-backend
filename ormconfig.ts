// eslint-disable-next-line @typescript-eslint/no-var-requires
const { join } = require('path');

module.exports = [
  {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    synchronize: false,
    migrationsRun: false,
    entities: [join(__dirname, '**', '*.entity.*')],
    migrations: [join(__dirname, 'src', 'db', 'migrations/*')],
    cli: {
      migrationsDir: 'src/db/migrations',
    },
  },
  {
    name: 'seed',
    type: 'postgres',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    synchronize: false,
    migrationsRun: false,
    entities: [join(__dirname, '**', '*.entity.*')],
    migrations: [join(__dirname, 'src', 'db', 'seeds/*')],
    cli: {
      migrationsDir: 'src/db/seeds',
    },
  },
];
