
## Before running

 add variables to .env file (example see in .env.example)

## Running the app

```bash
# development
$ docker-compose up
```

## Default admin credentials

Seeders will create default admin on first application start and credentials will be:

email - 'admin@email.com' 

password - '12345678'

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
